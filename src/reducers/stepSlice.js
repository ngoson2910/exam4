import { createSlice } from "@reduxjs/toolkit";


const stepSlice = createSlice({
  name: "step",
  initialState: {
    currentStep: 0
  },
  reducers: {
    setStep(state, action) {
      state.currentStep = action.payload
    }
  }
})

const {actions, reducer} = stepSlice
export const { setStep } = actions
export default reducer
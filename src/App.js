import React, { Component, Suspense } from 'react'
import { connect } from 'react-redux';
import {BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import Header from "./components/Header";
import Login from './pages/Login';
import NotFound from './pages/NotFound';
import StepByStep from './pages/StepByStep';

// lazy load
const Home = React.lazy(() => import('./pages/Home'))

class App extends Component{
  render () {
    const {user} = this.props
    return (
      <Suspense fallback={<div>Loading...</div>}>
        <BrowserRouter>
          <Header />
          <Routes>
            <Route path="/" element={<Home />} />
            {user
            ? (<Route path="/login" element={<Navigate to="/" />} />)
            : (<Route path="/login" element={<Login />} />)
            }
            
            <Route path="/step" element={<StepByStep />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </BrowserRouter>
      </Suspense>
    )
  }
}

const mapStateToProps = state => state.auth

export default connect(mapStateToProps)(App);

import React, { Component } from 'react'
import Banner from '../components/Banner'

class NotFound extends Component {
  render() {
    return (
      <div>
        <Banner title="Oppss....Not Found"  />
      </div>
    )
  }
}

export default NotFound

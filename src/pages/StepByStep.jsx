import React, { Component } from "react";
import { connect } from "react-redux";
import Banner from "../components/Banner";
import Completed from "../components/StepByStep/Completed";
import StepFour from "../components/StepByStep/StepFour";
import StepHeader from "../components/StepByStep/StepHeader";
import StepOne from "../components/StepByStep/StepOne";
import StepThree from "../components/StepByStep/StepThree";
import StepTwo from "../components/StepByStep/StepTwo";
import Image from "../constants/image";
import {STEP_ONE, STEP_TWO, STEP_THREE, STEP_FOUR} from '../constants/step'

class StepByStep extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    const { currentStep } = this.props.step
    const { completed } = this.props.complete
    return (
      <div>
        <Banner title="Step by Step" backgroundUrl={Image.PINK_BG} />
        <div className="container">
          <div className="row">
            <div className="col-md-8 offset-md-2">
              {completed ? (
                <Completed />
              ) : (
                <>
                  <StepHeader />
                  <div className="row">
                    <div className="col-md-6 offset-md-3">
                      {currentStep === STEP_ONE && <StepOne />}
                      {currentStep === STEP_TWO && <StepTwo />}
                      {currentStep === STEP_THREE && <StepThree />}
                      {currentStep === STEP_FOUR && <StepFour />}
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => state

export default connect(mapStateToProps)(StepByStep);

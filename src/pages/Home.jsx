import React, { Component } from "react";
import { connect } from "react-redux";
import Banner from "../components/Banner";

class Home extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { user } = this.props;
    return (
      <div>
        <Banner title="Home" />
        <div className="container mt-4">
          <div className="row">
            <div className="col-md-12">
              {user && (<h1>Hello, {user.username}</h1>)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => state.auth;

export default connect(mapStateToProps)(Home);

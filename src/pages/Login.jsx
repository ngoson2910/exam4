import React, { Component } from "react";
import { FastField, Form, Formik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import withRouter from "../components/HOC/withRouter";
import { login } from "../reducers/authSlice";
import InputField from "../customFields/InputField.js";
import Banner from "../components/Banner";
import Image from "../constants/image";

class Login extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { loading, error, onSubmit } = this.props;
    const initialValues = {
      username: "",
      password: "",
      passwordConfirmation: "",
    };
    const validationSchema = Yup.object({
      password: Yup.string().oneOf(
        [Yup.ref("passwordConfirmation"), null],
        ""
      ),
      passwordConfirmation: Yup.string().oneOf(
        [Yup.ref("password"), null],
        "Password must match"
      ),
    });
    return (
      <div>
        <Banner title="Login" backgroundUrl={Image.PINK_BG} />
        <div className="container mt-4">
          <div className="row">
            <div className="col-md-6 offset-md-3">
              {error && (
                <div className="alert alert-danger" role="alert">
                  {error}
                </div>
              )}
              <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={(values) => {
                  onSubmit(values);
                }}
              >
                {(formikProps) => {
                  return (
                    <Form>
                      <FastField
                        name="username"
                        component={InputField}
                        label="Username"
                        placeholder="Enter your username"
                      />
                      <FastField
                        name="password"
                        component={InputField}
                        label="Password"
                        type="password"
                        placeholder="Enter your password"
                      />
                      <FastField
                        name="passwordConfirmation"
                        component={InputField}
                        label="Confirm Password"
                        type="password"
                        placeholder="Confirm your password"
                      />
                      <div className="form-group text-end">
                        <button
                          type="submit"
                          className="btn btn-primary w-100 text-uppercase d-flex align-items-center justify-content-center"
                        >
                          {loading && (
                            <span
                              className="spinner-border text-light"
                              role="status"
                            >
                              <span className="visually-hidden">
                                Loading...
                              </span>
                            </span>
                          )}
                          Login
                        </button>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => state.auth;
const mapDispatchToEvent = (dispatch) => ({
  onSubmit: (values) => {
    dispatch(login(values));
  },
});

export default connect(mapStateToProps, mapDispatchToEvent)(withRouter(Login));

import React, { Component } from "react";
import PropTypes from "prop-types";

class CheckboxField extends Component {
  render() {
    const { field, form, type, label, disabled, checked } = this.props;
    const { name } = field;
    const { errors, touched } = form;
    const showError = errors[name] && touched[name];
    return (
      <div className="form-check">
        <input
          className="form-check-input"
          type={type}
          checked={checked}
          disabled={disabled}
          id={name}
          {...field}
        />
        {label && (
          <label className="form-check-label" htmlFor={name}>
            {label}
          </label>
        )}

        {showError && <p style={{ color: "red" }}>{errors[name]}</p>}
      </div>
    );
  }
}

CheckboxField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
};

CheckboxField.defaultProps = {
  type: "checkbox",
  label: "",
  checked: false,
  disabled: false,
};

export default CheckboxField;

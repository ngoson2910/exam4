import React, {Component} from 'react'
import PropTypes from 'prop-types'

class InputField extends Component {
  render() {
    const {
      field, form,
      type, label, placeholder, disabled
    } = this.props
  
    const { name }  = field;
    const { errors, touched } = form
    const showError = errors[name] && touched[name]
    return (
      <div className='form-group mb-3'>
        {label && <label className='mb-3' htmlFor={name}>{label}</label>}
        <input 
          type={type}
          id={name}
          className='form-control'
          {...field}
          disabled={disabled}
          placeholder={placeholder}
        />
        {showError && <p className='mt-2' style={{color: 'red'}}>{errors[name]}</p>}
      </div>
    )
  }
  
}
InputField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool
}

InputField.defaultProps = {
  type: 'text',
  label: '',
  placeholder: '',
  disabled: false
}

export default InputField;
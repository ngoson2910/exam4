import React, { Component } from "react";
import { FastField, Form, Formik } from "formik";
import { connect } from "react-redux";

import * as Yup from "yup";
import PropTypes from "prop-types";
import InputField from "../../../customFields/InputField";
import "./StepThree.css";
import { STEP_FOUR, STEP_TWO } from "../../../constants/step";
import { setFormValue } from "../../../reducers/formSlice";
import { setStep } from "../../../reducers/stepSlice";

class StepThree extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    const { password, confirmPassword, onSubmit, handlePreviusStep } = this.props
    const initialValues = {
      password: password,
      confirmPassword: confirmPassword,
    };

    const validationSchema = Yup.object().shape({
      password: Yup.string().required("This field is required!"),
      confirmPassword: Yup.string().oneOf(
        [Yup.ref("password")],
        "Password must match"
      ),
    });

    return (
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          onSubmit(values)
        }}
      >
        {(formikProps) => {
          return (
            <Form>
              <FastField
                name="password"
                component={InputField}
                type="password"
                label="Password"
                placeholder="Enter your password"
              />

              <FastField
                name="confirmPassword"
                component={InputField}
                type="password"
                label="Confirm password"
                placeholder="Confirm password"
              />
              <div className="form-group d-flex justify-content-end">
                <button
                  type="button"
                  className="btn btn-info me-3"
                  onClick={handlePreviusStep}
                >
                  Previous
                </button>
                <button type="submit" className="btn btn-primary">
                  Next
                </button>
              </div>
            </Form>
          );
        }}
      </Formik>
    );
  }
}

StepThree.propTypes = {
  onSubmit: PropTypes.func,
};

StepThree.defaultProps = {
  onSubmit: null,
};

const mapStateToProps = state => state.form
const mapDispatchToEvent = dispatch => ({
  onSubmit: (values) => {
    dispatch(
      setFormValue({
        password: values.password,
        confirmPassword: values.confirmPassword,
      })
    );
    dispatch(setStep(STEP_FOUR));
  }, 
  handlePreviusStep: () => {
    dispatch(setStep(STEP_TWO));
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToEvent
)(StepThree);

import  React, { Component} from 'react'
import { connect } from 'react-redux'
import './Completed.css'

class Completed extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    const {firstName, lastName, email} = this.props
    return(
      <>
        <h1>Completed</h1>
        <p>First Name: {firstName}</p>
        <p>Last Name: {lastName}</p>
        <p>Email: {email}</p>
      </>
    )
  }
  
}

const mapStateToProps = state => state.form

export default connect(mapStateToProps)(Completed)
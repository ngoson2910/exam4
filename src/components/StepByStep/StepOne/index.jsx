import React, { Component } from "react";
import { FastField, Form, Formik } from "formik";
import * as Yup from "yup";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import InputField from "../../../customFields/InputField";
import "./StepOne.css";
import { setFormValue } from "../../../reducers/formSlice";
import { setStep } from "../../../reducers/stepSlice";
import { STEP_TWO } from "../../../constants/step";

class StepOne extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    const { firstName, lastName, onSubmit} = this.props
    const initialValues = {
      firstName: firstName,
      lastName: lastName,
    };

    const validationSchema = Yup.object().shape({
      firstName: Yup.string().required("This field is required!"),
      lastName: Yup.string().required("This field is required!"),
    });
    return (
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          onSubmit(values)
        }}
      >
        {(formikProps) => {
          return (
            <Form>
              <FastField
                name="firstName"
                component={InputField}
                label="First Name"
                placeholder="First Name"
              />

              <FastField
                name="lastName"
                component={InputField}
                label="Last Name"
                placeholder="Last Name"
              />
              <div className="form-group d-flex justify-content-end">
                <button type="submit" className="btn btn-primary">
                  Next
                </button>
              </div>
            </Form>
          );
        }}
      </Formik>
    );
  }
}
StepOne.propTypes = {
  onSubmit: PropTypes.func,
};

StepOne.defaultProps = {
  onSubmit: null,
};

const mapStateToProps = state => state.form
const mapDispatchToEvent = dispatch => ({
  onSubmit: (values) => {
    dispatch(
      setFormValue({
        firstName: values.firstName,
        lastName: values.lastName,
      })
    );

    dispatch(setStep(STEP_TWO));
  },
})

export default connect(
  mapStateToProps,
  mapDispatchToEvent
)(StepOne);

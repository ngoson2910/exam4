import React, { Component } from 'react'
import { FastField, Form, Formik } from 'formik';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import * as Yup from 'yup'
import { complete } from '../../../reducers/completeSlice';
import { setFormValue } from '../../../reducers/formSlice';
import { setStep } from '../../../reducers/stepSlice';
import { STEP_THREE } from '../../../constants/step';
import CheckboxField from '../../../customFields/CheckboxField';
import './StepFour.css'


class StepFour extends Component {
  constructor(props, onSubmit, handlePreviusStep) {
    super(props)
  }
  render() {
    const {accept, onSubmit, handlePreviusStep} = this.props
    const initialValues = {
      check: accept
    }
    const validationSchema = Yup.object().shape({
      check: Yup.boolean()
    })
    return (
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={values => {
          onSubmit(values)
        }}
      >
        {(formikProps) => {
          const {values} = formikProps
          return (
            <Form>
              <div className="ten columns terms">
                <span>By clicking "Accept" I agree that:</span>
                <ul className="docs-terms">
                  <li>
                    I have read and accepted the<a href="#">User Agreement</a>
                  </li>
                  <li>
                    I have read and accepted the<a href="#">Privacy Policy</a>
                  </li>
                  <li>I am at least 18 years old</li>
                </ul>
                <FastField 
                  name="check"
                  checked={values.check}
                  component={CheckboxField}
                  label="Accept"
                />
              </div>
              <div className="form-group d-flex justify-content-end">
                <button type="button" className="btn btn-info me-3" onClick={handlePreviusStep}>
                  Previous
                </button>
                <button type="submit" className="btn btn-primary">
                  Submit
                </button>
              </div>
            </Form>
          );
        }}
      </Formik>
    )
  }
}
StepFour.propTypes = {
  onSubmit: PropTypes.func
}

StepFour.defaultProps = {
  onSubmit: null
}

const mapStateToProps = state => state.form
const mapDispatchToEvent = dispatch => ({
  onSubmit: (values) => {
    dispatch(setFormValue({
      accept: values.check
    }))
    dispatch(complete(true))
  },
  handlePreviusStep: () => {
    dispatch(setStep(STEP_THREE))
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToEvent
)(StepFour);
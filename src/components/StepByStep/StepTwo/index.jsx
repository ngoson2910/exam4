import React, { Component } from "react";
import { FastField, Form, Formik } from "formik";
import * as Yup from "yup";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import InputField from "../../../customFields/InputField";
import "./StepTwo.css";
import { setStep } from "../../../reducers/stepSlice";
import { setFormValue } from "../../../reducers/formSlice";
import { STEP_ONE, STEP_THREE } from "../../../constants/step";

class StepTwo extends Component {
  constructor(props, onSubmit, handlePreviusStep) {
    super(props)
    this.email = this.props.email
    this.confirmEmail = this.props.confirmEmail
    this.onSubmit = onSubmit
    this.handlePreviusStep = handlePreviusStep
  }
  render() {
    const {email, confirmEmail, onSubmit, handlePreviusStep} = this.props
    const initialValues = {
      email: email,
      confirmEmail: confirmEmail,
    };

    const validationSchema = Yup.object().shape({
      email: Yup.string().email().required("This field is required!"),
      confirmEmail: Yup.string()
        .email()
        .oneOf([Yup.ref("email")], "Email must match"),
    });
    return (
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values) => {
          onSubmit(values)
        }}
      >
        {(formikProps) => {
          return (
            <Form>
              <FastField
                name="email"
                component={InputField}
                label="Your email"
                placeholder="test@mailbox.com"
              />

              <FastField
                name="confirmEmail"
                component={InputField}
                label="Confirm email"
                placeholder="Confirm email"
              />
              <div className="form-group d-flex justify-content-end">
                <button
                  type="button"
                  className="btn btn-info me-3"
                  onClick={handlePreviusStep}
                >
                  Previous
                </button>
                <button type="submit" className="btn btn-primary">
                  Next
                </button>
              </div>
            </Form>
          );
        }}
      </Formik>
    );
  }
}

StepTwo.propTypes = {
  onSubmit: PropTypes.func,
};

StepTwo.defaultProps = {
  onSubmit: null,
};

const mapStateToProps = state => state.form
const mapDispatchToEvent = dispatch => ({
  onSubmit: (values) => {
    dispatch(
      setFormValue({
        email: values.email,
        confirmEmail: values.confirmEmail,
      })
    );
    dispatch(setStep(STEP_THREE));
  },
  handlePreviusStep: () => {
    dispatch(setStep(STEP_ONE));
  }
})

export default connect(
  mapStateToProps,
  mapDispatchToEvent
)(StepTwo);

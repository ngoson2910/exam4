import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import withRouter from "../HOC/withRouter";
import { logout } from "../../reducers/authSlice";
import "./Header.css";

class Header extends Component {
  constructor(props) {
    super(props);
    this.logout = this.props.logout;
  }
  handleLogout = () => {
    localStorage.removeItem("user");
    this.logout();
    this.props.navigate("/login");
  };
  render() {
    const { user } = this.props;
    return (
      <nav className="navbar navbar-light bg-light">
        <div className="container">
          <Link className="navbar-brand" to="/">
            Redux Toolkits
          </Link>
          <div>
            {user ? (
              <button className="btn btn-success d-inline-block me-3" onClick={this.handleLogout}>
                Logout
              </button>
            ) : (
              <NavLink
                to="/login"
                className={(navData) =>
                  navData.isActive
                    ? "header-link active me-3"
                    : "header-link me-3"
                }
              >
                Login
              </NavLink>
            )}

            <NavLink
              to="/step"
              className={(navData) =>
                navData.isActive ? "header-link active" : "header-link"
              }
            >
              StepByStep
            </NavLink>
          </div>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = (state) => state.auth;
const mapDispatchToEvent = (dispatch) => ({
  logout: () => {
    dispatch(logout());
  },
});

export default connect(mapStateToProps, mapDispatchToEvent)(withRouter(Header));

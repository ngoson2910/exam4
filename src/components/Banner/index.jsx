import React, { Component } from 'react'
import './Banner.css'

class Banner extends Component {
  render() {
    const {title, backgroundUrl} = this.props;
    const bannerStyle = backgroundUrl
      ? {backgroundImage: `url(${backgroundUrl})`}
      : {}
    return (
      <div className='banner' style={bannerStyle}>
        <h1 className='banner-title'>{title}</h1>
      </div>
    )
  }
}

export default Banner

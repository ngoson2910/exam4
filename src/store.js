import { configureStore } from "@reduxjs/toolkit";
import formSlice from './reducers/formSlice'
import stepSlice  from './reducers/stepSlice'
import completeSlice from './reducers/completeSlice'
import authSlice from './reducers/authSlice'


const store = configureStore({
  reducer: {
    form: formSlice,
    step: stepSlice,
    complete: completeSlice,
    auth: authSlice
  }
})

export default store